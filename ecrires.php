<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Ecrire</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        function show(){
            document.getElementById('hide').style.display='block';
        }
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/entetes.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
					<?php
				$query="select t.code_tercero,t.code,e.matricule,t.nom,t.prenom,cl.description,cl.section from inscription i left join eleve e 
				on( e.code=i.code and e.code_tercero = i.code_tercero) left join classe cl on i.code_classe = cl.code_classe left join tercero t 
				on( i.code=t.code and i.code_tercero = t.code_tercero)  where e.matricule='$matricule'";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[5]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-edit"></i> Ecrire un nouveau message
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="" role="form" enctype="multipart/form-data">

                            <div class="form-group" >
                               <label> <a href="#" onclick="show()" ><i class="fa fa-paperclip 
                                   "></i> Ins&eacute;rer un fichier</a></label>
                                <hr/>
                             </div> 
							
                            <div class="form-group" id="hide" style="display:none;">
                                 <label>Joindre fichier(DOCS, XLS, CSV, PPT, PDF, RAR, ZIP, ISO)*</label>
                                  <input type="file" class="form-control" name="doc" id="doc">
                             </div>         
                            <div class="form-group">
                                            <label>Matricule du Professeur*</label>
                                            <input type="text" class="form-control" placeholder="Matricule" name="objets">
                                        </div>
                                        <div class="form-group">
                                            <label>Objet*</label>
                                            <input type="text" class="form-control" placeholder="Objet du message" name="objet">
                                        </div>
                                        <div class="form-group">
                                            <label>Message*</label>
                                            <textarea class="form-control" rows="7" name="message"></textarea>
                                        </div>
                            <div class="panel-body">
                                 <button type="submit" class="btn btn-primary btn-lg btn-block" name="envoyer">Envoyer</button>
                            </div>
                           <?php
                            if(isset($_POST['envoyer'])){
                                
                                    $sobjets=addslashes($_POST['objets']);
                                    $objets=addslashes($_POST['objet']);
                                    $messages=addslashes($_POST['message']);
                                   $archivo=$_FILES["doc"]["tmp_name"];
                                   $destino="docs/".$_FILES['doc']['name'];
                                   move_uploaded_file($archivo, $destino);
                                 $query="insert into message(classe,matricule,objet,messages,lien,annee_academique) values('$rows[5]','$matricule','$objets','$messages','$destino','$annee')";
                                    $res=ejecutar_query($query,$link);
                                    $cmd="select max(id_message) as id from message";
                                    $lol=ejecutar_query($cmd,$link);
                                    $kod=traer_fila($lol);
                                    $str="insert into detaille_message values('$kod[0]','$sobjets','Non Lu')";
                                    $aji=ejecutar_query($str,$link);
                              }
                            ?>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
