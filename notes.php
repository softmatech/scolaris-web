<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$codhoraire='HorProf#'.$matricule;
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
$klas=$_GET['classe'];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Consultation</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select deviz from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[0]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-search"></i> Consultation des Notes 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
                          <div class="panel-body">  
                           <a href="consulter_note.php?matricule=<?php echo $matricule;?>" title="Retour a la page precedente">
                               <i class="fa fa-mail-reply "></i> Retour à la page précédente pour choisir  une Classe</a> 
                              <hr/>
                            </div>
                                
                            
                             <div class="col-lg-8">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                <i class="fa fa-users"></i> <label> Matricules par Classe / Contrôle*</label>
                                </div>
                                    <div class="panel-body">
                    
                                            <fieldset>
                                 <input class="form-control" placeholder="Classe" name="classes" type="text" value="<?php echo $klas;?>" readonly>
                                 <input class="form-control" placeholder="Classe" name="classes" type="hidden" value="<?php echo $klas;?>">
                                <div class="form-group input-group">
                                    <?php
                                    $sql="select i.matricule from inscription i left join classe cl on cl.code_classe=i.code_classe where 
                                     i.annee_academique='$annee' and cl.description='$klas'";
                                    $wouch=ejecutar_query($sql,$link);
                                    ?>
                                <select class="form-control" name="eleve" values="">
                                      <option>Choisir le matricule de l'&eacute;l&egrave;ve...</option>
                                        <?php for($j=1;$srow=traer_fila($wouch);$j++) { ?>
                                        <option><?php echo $srow[0];?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit" name="filtrernote"><i class="fa fa-eye"></i>
                                    Afficher Note</button>
                                    </span>
                                    
                                    </div>
                                      </fieldset>

                                    </div>
                                </div>
                            </div>
                            
         <div class="row"></div>
                    
                              <div class="panel-body">
                                <input type="button" class="btn btn-default btn-lg btn-block" value="Notes des &eacute;l&egrave;ves">
                                </div>  
                            
                            <?php
$query="select t.matricule,t.annee,t.matiere,t.sur,t.controle1,t.controle2,t.controle3,t.controle4,cl.description from temp t 
left join note n on n.matricule=t.matricule left join classe cl on cl.code_classe=n.code_classe where cl.description='$klas' and t.annee='$annee' group by matricule"; 
$kouri=ejecutar_query($query,$link);
                                    
         if(isset($_POST['filtrernote'])){ 
             $eleves=$_POST['eleve'];
             if($eleves!="Choisir le matricule de l'élève..."){
$query="select t.matricule,t.annee,t.matiere,t.sur,t.controle1,t.controle2,t.controle3,t.controle4,cl.description from temp t 
left join note n on n.matricule=t.matricule left join classe cl on cl.code_classe=n.code_classe where t.matricule='$eleves' and cl.description='$klas' and t.annee='$annee' group by matricule"; 
$kouri=ejecutar_query($query,$link);
             }else{
        $query="select t.matricule,t.annee,t.matiere,t.sur,t.controle1,t.controle2,t.controle3,t.controle4,cl.description from temp t 
left join note n on n.matricule=t.matricule left join classe cl on cl.code_classe=n.code_classe where cl.description='$klas' and t.annee='$annee' group by matricule"; 
$kouri=ejecutar_query($query,$link);  
             }
                             }
                            for($i=1;$fil=traer_fila($kouri);$i++){
                            ?>
                            <div class="col-lg-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <i class="fa fa-user"></i> <?php echo $fil[0];?>
                                    </div>
                              <div class="panel-body"> 
                            <fieldset>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead style="background-color:rgb(87,148,210);">
                                    <tr>
                                         <th > &nbsp;Actions&nbsp;</th>
										  <th> &nbsp;Mati&egrave;res&nbsp;</th>
										  <th> &nbsp;Sur&nbsp;</th>
                                          <th> &nbsp;Controle 1&nbsp;</th>
										  <th> &nbsp;Controle 2&nbsp;</th>
										  <th> &nbsp;Controle 3&nbsp;</th>
                                        <th> &nbsp;Controle 4&nbsp;</th>
                                    </tr>
                                </thead>
                                
             <?php
 $query="select t.matricule,t.annee,t.matiere,t.sur,t.controle1,t.controle2,t.controle3,t.controle4,cl.description from temp t 
left join note n on n.matricule=t.matricule left join classe cl on cl.code_classe=n.code_classe where t.matricule='$fil[0]'group by matiere "; 
$kourii=ejecutar_query($query,$link);
                                for($i=1;$fils=traer_fila($kourii);$i++){
                                ?>
                       
								 <tr>
                                     <td width="40">&nbsp;<a href="korijenot.php?matricule=<?php echo $matricule;?>&eleve=<?php echo $fils[0];?>&equi=<?php echo $fils[3];?>&uns=<?php echo $fils[4];?>&des=<?php echo $fils[5];?>&tres=<?php echo $fils[6];?>&unos=<?php echo $fils[7];?>&mat=<?php echo $fils[2];?>" target="_parent" title="Editer notes"><i class="fa fa-edit "></i></a></td>
								<td>&nbsp;<?php echo $fils[2];?></td>
								<td width="120">&nbsp;<?php echo $fils[3];?></td>
								<td width="120">&nbsp;<?php echo $fils[4];?></td>
								<td width="120">&nbsp;<?php echo $fils[5];?></td>
                                <td width="120">&nbsp;<?php echo $fils[6];?></td>
                                <td width="120">&nbsp;<?php echo $fils[7];?></td>
                                     
								</tr>
                                  
                                 <?php } ?>                        
                            </table>
                            </fieldset>
                           
                                    </div>
                                    
                                </div>
                             </div> 
                          <?php }?>  
                            
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    

</body>

</html>
