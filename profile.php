<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
  <link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <title>Scolaris | Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
	<style>
input[type=text],input[type=mail] {
 width: 100%;
 padding: 12px 20px;
 margin: 8px 0;
 box-sizing: border-box;
}
select{
 width: 100%;
 padding: 12px 20px;
 margin: 8px 0;
 box-sizing: border-box;
}
input[type=button], input[type=submit], input[type=reset] {
    background-color: #0099FF;
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 2px;
    cursor: pointer;
	width: 100%;
}
</style>

	</head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <?php include("includes/enteteprive.php"); ?>
  </nav>
    <!-- END nav -->

       <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
        			<div class="col-md-12 mb-5">
        				<div class="teacher-details d-md-flex">
        					<div class="img ftco-animate" ><?php include("includes/menu.php"); ?></div>
        					<div class="col-md-8">
								<form method="post" action="" >
						   <?php
							$sql="select t.nom,t.prenom,p.date_naissance,p.lieu_naissance,p.sexe,s.sang,e.cond_sante,p.adresse,te.mail,tt.phone_number,c.nom,c.prenom,
							c.telephone,er.lien_parente,e.matricule from eleve e left join tercero t on ((t.code=e.code) and (t.code_tercero=e.code_tercero)) 
							left join personal_data p on ((p.code=e.code) and (p.code_tercero=e.code_tercero)) 
							left join contact_urgence c on ((c.code=e.code) and (c.code_tercero=e.code_tercero)) 
							left join tercero_email te on ((te.code=e.code) and (te.code_tercero=e.code_tercero)) 
							left join tercero_vs_telephones tt on ((tt.code=e.code) and (tt.code_tercero=e.code_tercero)) 
							left join eleve_has_tercero_responsable er on (er.eleve_matricule=e.matricule)
							left join sanguins s on ((s.code=e.code) and (s.code_tercero=e.code_tercero)) where e.matricule='$matricule'";
							$res=ejecutar_query($sql,$link);
							$row=traer_fila($res);
						   ?>
							<table>
							  <thead>
								<tr>
								  <th>Modifications des Informations</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								</tr>
							  </tbody>
							</table>
							<label>Les informations avec le(<span style="color:red;">*</span>) en rouge ne peuvent pas modifier.</label>
							 <label>Nom<span style="color:red;">*</span></label>
							<input type="text" name="nom" id="nom" value="<?php echo $row[0];?>" placeholder="Nom*" readonly>
							<label>Prénom<span style="color:red;">*</span></label>
							<input type="text" name="prenom" id="prenom" value="<?php echo $row[1];?>" placeholder="Prénom*" readonly>
							<label>Date de naissance<span style="color:red;">*</span></label>
							<input type="text" name="naissance" id="naissance" value="<?php echo $row[2];?>" placeholder="Date de Naissance*" readonly>
							<label>Lieu de naissance<span style="color:red;">*</span></label>
							<input type="text" name="lieu" id="lieu" value="<?php echo $row[3];?>" placeholder="Lieu de Naissance*" readonly>
							<label>Sèxe<span style="color:red;">*</span></label>
							<input type="text" name="sexe" id="lieu" value="<?php echo $row[4];?>" placeholder="Sexe*" readonly>
							<label>Groupe Sanguins<span style="color:red;">*</span></label>
							<input type="text" name="sang" id="lieu" value="<?php echo $row[5];?>" placeholder="Groupe Sanguin*" readonly>
							<label>Condition Santé</label>
							<select name="sante" id="sante" >
							<option><?php echo $row[6];?></option>
							<option>Allèrgique</option>
							<option>Non Allèrgique</option>
							<option>Malaria</option>
							<option>Typhoïde</option>
							<option>Typho-Malaria</option>
							<option>Autres</option>
							</select>
							<label>Adrèsse</label>
							<input type="text" name="adresse" id="adresse" value="<?php echo $row[7];?>" placeholder="Adresse*">
							<label>Courrier Eléctronique</label>
							<input type="mail" name="email" id="email" value="<?php echo $row[8];?>" placeholder="E-mail*">
							<label>Téléphone</label>
							<input type="text" name="tel" id="tel" value="<?php echo $row[9];?>" placeholder="Téléphone*">
							<label>Nom du responsable<span style="color:red;">*</span></label>
							<input type="text" name="nresponsable" id="responsable" value="<?php echo $row[10];?>" placeholder="Nom du responsable*" readonly>
							<label>Prénom du responsable<span style="color:red;">*</span></label>
							<input type="text" name="presponsable" id="responsable" value="<?php echo $row[11];?>" placeholder="Prénom du responsable*" readonly>
							<label>Téléphone du responsable<span style="color:red;">*</span></label>
							<input type="text" name="telephone" id="telephone" value="<?php echo $row[12];?>" placeholder="Téléphone du Responsable*" readonly>
							<label>Lien Parenté<span style="color:red;">*</span></label>
							<input type="text" name="lien" id="telephone" value="<?php echo $row[13];?>" placeholder="Téléphone du Responsable*" readonly>		
							<input type="submit" name="submit" id="submit" value="Modifier" >
							</form>
        					</div>
        				</div>
        			</div>
        			
        		</div>
        	</div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section img" style="background-image: url(images/bg_2.jpg); background-attachment:fixed;" >
    	<?php include("includes/pie.php"); ?>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>