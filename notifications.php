<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Programme</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/entetes.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select t.code_tercero,t.code,e.matricule,t.nom,t.prenom,cl.description,cl.section from inscription i left join eleve e 
				on( e.code=i.code and e.code_tercero = i.code_tercero) left join classe cl on i.code_classe = cl.code_classe left join tercero t 
				on( i.code=t.code and i.code_tercero = t.code_tercero)  where e.matricule='$matricule'";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[5]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                          <i class="fa fa-tasks"></i> Programme de classe
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
									  <?php 
									   $cmd="select cl.description from classe cl left join inscription i on i.code_classe=cl.code_classe
									   where i.matricule='$matricule'";
									   $rez=ejecutar_query($cmd,$link);
									   $wow=traer_fila($rez);
										$query="select cl.description ,p.matiere,p.chapitre from programme p left join matiere_vs_classe cm
									   on cm.code_classe = p.code_classe left join classe cl on cl.code_classe = cm.code_classe where cl.description='$wow[0]' group by matiere";
										$ser=ejecutar_query($query,$link);
									 ?>
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
								<?php for($i=1;$wor=traer_fila($ser);$i++){?>
							   <thead style="background-color:rgb(87,148,210);">
                                    <tr>
                                        <th><?php echo $wor[1];?></th>
                                    </tr>
                                </thead>
								 <?php 
									$sql="select cl.description ,p.matiere,p.chapitre from programme p left join matiere_vs_classe cm
									on cm.code_classe = p.code_classe left join classe cl on cl.code_classe = p.code_classe where p.matiere='$wor[1]' group by chapitre";
									$resss=ejecutar_query($sql,$link);
									for($j=1;$rowss=traer_fila($resss);$j++){
								?>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td><?php echo $rowss[2];?></td>
                                    </tr>
                                </tbody>
								<?php }}?>
                            </table>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
