<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$root=$_GET['root'];
$lane=$_GET['lane'];
$objet=$_GET['objet'];
$query="update detaille_message dn left join message n on n.id_message=dn.id_message
set dn.status='Deja Lu' where dn.matricule='$matricule' and n.objet='$objet' and n.annee_academique='$lane'";
$ress=ejecutar_query($query,$link);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Message</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/entetes.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select m.objet,m.messages,t.nom,t.prenom,m.lien,date_format(m.dates,'%d %M %Y / %h:%m:%s') as dates,m.annee_academique,
                dm.matricule as eleve,dm.status from message m left join detaille_message dm on dm.id_message=m.id_message 
                left join employer e on e.matricule=m.matricule left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero)  where dm.matricule='$matricule' and m.objet='$objet'";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[5]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-envelope fa-fw"></i> <?php echo $objet;?>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
						<?php
							$query="select m.objet,m.messages,t.nom,t.prenom,m.lien,date_format(m.dates,'%d %M %Y /  %h:%m:%s') as dates,m.annee_academique,
                            dm.matricule as eleve,dm.status,m.matricule,m.dates as fecha from message m left join detaille_message dm on dm.id_message=m.id_message 
                            left join employer e on e.matricule=m.matricule left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero) where dm.matricule='$matricule' and m.objet='$objet'";
								$res=ejecutar_query($query,$link);
								$row=traer_fila($res);
                            if($row[4]=="docs/"){
                                $fichier="";
                                $imaj="";
                            }else{
                                $fichier=$row[4];
                                $imaj="images/Export%20Pdf_100px.png";
                            }
						?>
							<div class="well">
                                <p><?php echo "Exp&eacute;diteur: ".$row[9]." | ".$row[2]." ".$row[3];?></p>
                                <hr/>
                                <h4><?php echo $row[1];?></h4>
                                <p><a  href="<?php echo $row[4];?>" target="_blank" title="T&eacute;l&eacute;charger le fichier."><img src="<?php echo $imaj;?>"></a>
                                </p>
                                <a class="btn btn-default btn-lg btn-block" target="_parent" href="del.php?matricule=<?php echo $matricule;?>&root=<?php echo $row[10];?>&lane=<?php echo $lane;?>&objet=<?php echo $objet;?>"><?php echo "Message ".$row[8]." / ";?><i class="fa fa-trash-o  "></i> </a>
                            </div>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>
