<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$codhoraire='HorProf#'.$matricule;
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
$equi=$_GET['equi'];
$mat=$_GET['mat'];
$cont1=$_GET['uns'];
$cont2=$_GET['des'];
$cont3=$_GET['tres'];
$cont4=$_GET['unos'];
$eleve=$_GET['eleve'];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <link href="images/icono.ico" type="image/x=icon" rel="shortcut icon" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Correction</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <?php
				$query="select deviz from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);">
                        <?php echo $rows[0]?>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-edit"></i> Modification des Notes
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <form method="post" action="">
                                <div class="col-lg-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <i class="fa fa-user"></i>
                                            <?php echo $eleve;?>
                                        </div>
                                        <div class="panel-body">
                                            <fieldset>
  <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">


<tr>
<td  width="220">&nbsp;<input class="form-control" placeholder="Matiere" name="classes" type="text" value="<?php echo $mat;?>" readonly>

<input class="form-control" placeholder="Matiere" name="classes" type="hidden" value="<?php echo $mat;?>">

</td>
 <td width="120">&nbsp;<input class="form-control" placeholder="Equivalence" name="eqr" type="text" value="<?php echo $equi;?>" readonly></td>
 <td width="120">&nbsp;<input class="form-control" placeholder="Controle 1" name="kont1" type="text" value="<?php echo $cont1;?>"></td>
 <td width="120">&nbsp;<input class="form-control" placeholder="Controle 2" name="kont2" type="text" value="<?php echo $cont2;?>" </td> <td width="120">&nbsp;<input class="form-control" placeholder="Controle 3" name="kont3" type="text" value="<?php echo $cont3;?>" </td> <td width="120">&nbsp;<input class="form-control" placeholder="Controle 4" name="kont4" type="text" value="<?php echo $cont4;?>"> </td> 
 </tr> 
 </table>
  <div class="panel-body">
<button type="submit" class="btn btn-primary btn-lg btn-block" name="envoyer">Modifier</button>
      </div>
               </fieldset>
             <?php
             if(isset($_POST['envoyer'])){ $mats=$_POST['classes'];
            $controle1=$_POST['kont1']; 
             $controle2=$_POST['kont2'];
            $controle3=$_POST['kont3'];
            $controle4=$_POST['kont4'];
$sql="update detaille_note set note='$controle1' where matiere='$mats' and controle=1 and annee='$annee' and matricule='$eleve' ";
$res=ejecutar_query($sql,$link);
    // echo $sql;
$sql="update detaille_note set note='$controle2' where matiere='$mats' and controle=2 and annee='$annee' and matricule='$eleve' ";
$res=ejecutar_query($sql,$link);
    // echo $sql;
 $sql="update detaille_note set note='$controle3' where matiere='$mats' and controle=3 and annee='$annee' and matricule='$eleve' ";
$res=ejecutar_query($sql,$link);
    // echo $sql;
 $sql="update detaille_note set note='$controle4' where matiere='$mats' and controle=4 and annee='$annee' and matricule='$eleve' ";
$res=ejecutar_query($sql,$link);
     //cho $sql; 
  
  $sql="update temp set controle1='$controle1',controle2='$controle2',controle3='$controle3',controle4='$controle4' where matiere='$mats'  and annee='$annee' and matricule='$eleve' ";
$res=ejecutar_query($sql,$link);
    // echo $sql;
$str="select sum(controle1) as cont1,sum(controle2) as cont2, sum(controle3) as cont3, sum(controle4) as cont4, sum(sur) as sur from temp 
where matricule='$eleve' and annee='$annee'";
$res=ejecutar_query($str,$link);
$ral=traer_fila($res); 
$moy1=$ral[0]/($ral[4]/10);
$moy2=$ral[1]/($ral[4]/10);
$moy3=$ral[2]/($ral[4]/10);
$moy4=$ral[3]/($ral[4]/10); 
$moys1= number_format($moy1,2,".",".");
$moys2= number_format($moy2,2,".",".");
$moys3= number_format($moy3,2,".",".");
$moys4= number_format($moy4,2,".",".");               
                 
 $cmd="update note set total_note='$ral[0]',moyenne='$moys1' where matricule='$eleve' and controle=1 and annee='$annee'";
    $rez=ejecutar_query($cmd,$link);
 $cmd="update note set total_note='$ral[1]',moyenne='$moys2' where matricule='$eleve' and controle=2 and annee='$annee'";
    $rez=ejecutar_query($cmd,$link); 
   $cmd="update note set total_note='$ral[2]',moyenne='$moys3' where matricule='$eleve' and controle=3 and annee='$annee'";
    $rez=ejecutar_query($cmd,$link);  
    $cmd="update note set total_note='$ral[3]',moyenne='$moys4' where matricule='$eleve' and controle=4 and annee='$annee'";
    $rez=ejecutar_query($cmd,$link);  
 header("Location:msg.php?matricule=$matricule");                                   
 
                              }
                            ?>
                                    </div>

                                </div>
                        </div>

                        </form>
                        <!-- /.table-responsive -->

                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!-- /.row -->

        <!-- /.row -->

        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->


</body>

</html>
