<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$sql="select l.valeur from limite_view_result l left join institution i on i.idinstitution=l.idinstitution ";
$res=ejecutar_query($sql,$link);
$row=traer_fila($res);

$cmd="select balance from collecter where matricule='$matricule' order by balance limit 1";
$result=ejecutar_query($cmd,$link);
$fila=traer_fila($result);
if($row[0]<$fila[0]){
	session_start();
 $_SESSION['matricule'] = $matricule;
 header("Location: resultats.php?matricule=$matricule");
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Resultat</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/entetes.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select t.code_tercero,t.code,e.matricule,t.nom,t.prenom,cl.description,cl.section from inscription i left join eleve e 
				on( e.code=i.code and e.code_tercero = i.code_tercero) left join classe cl on i.code_classe = cl.code_classe left join tercero t 
				on( i.code=t.code and i.code_tercero = t.code_tercero)  where e.matricule='$matricule'";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[5]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-table"></i> &nbsp;Résultat de Examens&nbsp;
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
									
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
							   <thead style="background-color:rgb(87,148,210);">
                                    <tr>
                                         <th scope="col">Mati&egrave;res&nbsp;</th>
										<th scope="col">Sur&nbsp;</th>
										<th scope="col">1er Contr&ocirc;le&nbsp;</th>
										<th scope="col">2&egrave;me Contr&ocirc;le&nbsp;</th>
										<th scope="col">3&egrave;me Contr&ocirc;le&nbsp;</th>
                                        <th scope="col">4&egrave;me Contr&ocirc;le&nbsp;</th>
                                    </tr>
                                </thead>
								  <?php 
											
										$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
										$res=ejecutar_query($query,$link);
										$row=traer_fila($res);
										$annee=$row[0];

										$query="select * from temp where matricule='$matricule' and annee='$annee'";
										$res=ejecutar_query($query,$link);
								  ?>
                                <tbody>
                                    <?php for($i=1;$row=traer_fila($res);$i++){ ?>
											<tr>
												<th align="right" ><?php echo $row[2];?></th>
												<td >&nbsp;<?php echo $row[3];?></td>
												<td align="center">&nbsp;<?php echo $row[4];?></td>
												<td align="center">&nbsp;<?php echo $row[5];?></td>
												<td align="center">&nbsp;<?php echo $row[6];?></td>
                                                <td align="center">&nbsp;<?php echo $row[7];?></td>
											</tr>
								<?php }?>
								<tr style="color:#00C;" bgcolor="#CCCCCC">
										   <?php 
											$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=1 group by matiere";
											$res=ejecutar_query($query,$link); 
											$row=traer_fila($res);
											$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe  where e.matricule='$matricule' and n.annee='$annee' and n.controle=2 group by matiere";
											$res=ejecutar_query($query,$link); 
											$rows=traer_fila($res);
											$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe  where e.matricule='$matricule' and n.annee='$annee' and n.controle=3 group by matiere";
											$res=ejecutar_query($query,$link); 
											$srow=traer_fila($res);
                                    $query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe  where e.matricule='$matricule' and n.annee='$annee' and n.controle=3 group by matiere";
											$res=ejecutar_query($query,$link); 
											$srows=traer_fila($res);
											?>
											<th scope="row" align="left" >&nbsp;ToTal&nbsp;</th>
											<td scope="row" align="center"><strong><?php echo $row[10];?></strong>&nbsp;</td>

											<td scope="row" align="center"><strong><?php echo $row[9];?></strong>&nbsp;</td>

											<td scope="row" align="center"><strong><?php echo $rows[9];?></strong>&nbsp;</td>

											<td align="center"><strong><?php echo $srow[9];?></strong>&nbsp;</td>
                                            <td align="center"><strong><?php echo $srows[9];?></strong>&nbsp;</td>

										</tr>
										<?php 
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=1 group by matiere";
										$res=ejecutar_query($query,$link); 
										$row=traer_fila($res);
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=2 group by matiere";
										$res=ejecutar_query($query,$link); 
										$rows=traer_fila($res);
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=3 group by matiere";
										$res=ejecutar_query($query,$link); 
										$srow=traer_fila($res);
                                    $query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=4 group by matiere";
										$res=ejecutar_query($query,$link); 
										$srows=traer_fila($res);
										?>
										<tr style="color:#F00;" bgcolor="#CCCCCC" align="center">
										<th scope="row" align="left" bgcolor="#CCCCCC">&nbsp;Moy&egrave;nne&nbsp;</th>
                                            <td scope="row" bgcolor="#CCCCCC"><strong>&nbsp;10</strong></td>

										<td scope="row" bgcolor="#CCCCCC"><strong><?php echo number_format($row[11],2,".",".");?></strong>&nbsp;</td>

										<td scope="row" bgcolor="#CCCCCC"><strong><?php echo number_format($rows[11],2,".",".");?></strong>&nbsp;</td>

                                            <td scope="row" bgcolor="#CCCCCC"><strong><?php echo number_format($srow[11],2,".",".");?></strong>&nbsp;</td>
                                            <td scope="row" bgcolor="#CCCCCC"><strong><?php echo number_format($srows[11],2,".",".");?></strong>&nbsp;</td>
									  </tr>
									  
									  <?php 
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=1 group by matiere";
										$res=ejecutar_query($query,$link); 
										$row=traer_fila($res);
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=2 group by matiere";
										$res=ejecutar_query($query,$link); 
										$rows=traer_fila($res);
										$query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=3 group by matiere";
										$res=ejecutar_query($query,$link); 
										$srow=traer_fila($res);
                                        $query="select e.matricule,t.nom ,t.prenom ,c.description,c.section,dn.matiere,dn.note,dn.sur
											,n.controle,n.total_note,n.total_sur,n.moyenne,n.status,n.annee,
											d.moyenne_generale ,d.estatus from note n left join detaille_note dn on
											dn.matricule = n.matricule left join eleve e on e.matricule = n.matricule 
											left join tercero t on (t.code=e.code and t.code_tercero = e.code_tercero) 
											left join decision d on  d.matricule = e.matricule
											left join classe c on c.code_classe = n.code_classe where e.matricule='$matricule' and n.annee='$annee' and n.controle=4 group by matiere";
										$res=ejecutar_query($query,$link); 
										$srows=traer_fila($res);
										?>
										<tr style="color:#060;" bgcolor="#CCCCCC" >
										<th scope="row" bgcolor="#CCCCCC">&nbsp;Qualification&nbsp;</th>
                                            <td colspan="2" align="right" scope="row" bgcolor="#CCCCCC"><strong><?php echo $row[12];?></strong>&nbsp;</td>

                                            <td scope="row" align="center" bgcolor="#CCCCCC"><strong><?php echo $rows[12];?></strong>&nbsp;</td>

                                            <td scope="row" align="center" bgcolor="#CCCCCC"><strong><?php echo $srow[12];?></strong>&nbsp;</td>
                                            <td scope="row" align="center" bgcolor="#CCCCCC"><strong><?php echo $srows[12];?></strong>&nbsp;</td>

									  </tr>
									  <tr style="color:#F09;" bgcolor="#CCCCCC" >
										<th scope="row" bgcolor="#CCCCCC">&nbsp;Moy. G&eacute;n&eacute;rale&nbsp;</th>
										<td colspan="4" scope="row" bgcolor="#CCCCCC" >&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo $srow[14];?></strong></td>
                                          <td colspan="4" scope="row" bgcolor="#CCCCCC" >&nbsp;&nbsp;&nbsp;&nbsp;<strong><?php echo $srow[14];?></strong></td>
								      </tr>
										<tr style="color:#600;" bgcolor="#CCCCCC" >
										<th scope="row" bgcolor="#CCCCCC">&nbsp;D&eacute;cision de fin d'ann&eacute;e&nbsp;</th>
										<td colspan="4" scope="row" bgcolor="#CCCCCC">&nbsp;<strong><?php echo $srow[15];?></strong></td>
                                        <td colspan="4" scope="row" bgcolor="#CCCCCC">&nbsp;<strong><?php echo $srow[15];?></strong></td>
									  </tr>
                                </tbody>
                            </table>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
