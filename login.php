<?php
include('lib/Classe.php');
$link=db_conectar();
session_start();
ob_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
	<title>Scolaris | Se Connecter</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post">
				<span class="contact100-form-title">
					Connectez-vous
				</span>


				<div class="wrap-input100 validate-input" data-validate="Le matricule est obligatoire">
					<label class="label-input100" for="name">Matricule</label>
					<input id="name" class="input100" type="text" name="matricule" placeholder="Elève Matricule..." >
					<span class="focus-input100"></span>
				</div>


				<div class="wrap-input100 validate-input" data-validate = "Le mot de passe est obligatoire">
					<label class="label-input100" for="email">Mot de passe</label>
					<input id="email" class="input100" type="password" name="password" placeholder="Elève Mot de passe...">
					<span class="focus-input100"></span>
				</div>
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn" name="connect"  style="background-color:rgb(87,148,210);">
						Se Connecter
					</button>
                     <?php
                        if(isset($_POST['connect'])){
                            $matricules=$_POST['matricule'];
                            $passwords=$_POST['password'];
                            $query="select code,code_tercero,matricule,securite from eleve where matricule=ltrim(rtrim('$matricules')) and securite=ltrim(rtrim('$passwords'))";
                            $res=ejecutar_query($query,$link);
                            $row=traer_fila($res);
                            if ($passwords==$row[3]) {
                                session_start();
                                $_SESSION['matricule'] = $row[2];
                                header("Location: notifications.php?matricule=$row[2]");
                            } else {
                                header("Location: login.php?matricule=$matricules");
                                exit();
                            }

                        }
                     ?>
				</div>

				<div class="contact100-form-social flex-c-m">
					<a href="index.php" class="contact100-form-social-item flex-c-m bg1 m-r-5" title="Retournez-vous a la page d'accueil.">
						<i class="fa fa-home" aria-hidden="true"></i>
					</a>
				</div>
			</form>

			<div class="contact100-more flex-col-c-m" style="background-image: url('images/log.jpeg');">
			</div>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
		$(".js-select2").each(function(){
			$(this).on('select2:open', function (e){
				$(this).parent().next().addClass('eff-focus-selection');
			});
		});
		$(".js-select2").each(function(){
			$(this).on('select2:close', function (e){
				$(this).parent().next().removeClass('eff-focus-selection');
			});
		});

	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/mainsoft.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
</body>
</html>
