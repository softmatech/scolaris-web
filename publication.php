<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$codhoraire='HorProf#'.$matricule;
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Publication</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
     function konpare(id,ids){
         var ekivalans=$('#id').val();
         var not=$('#ids').val();
         if(ekivalans<not){
             alert("Ouups! l'&eacute;quivalance ne doit pas inf&eacute;rieur au notre entr&eacute;");
             document.getElementById('note').nodeValue="";
         }
     }
    </script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select deviz,nom from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rejis=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rejis[0]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-bullhorn"></i> Publication des notes
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
                            <?php
                              $query="select classe from horaire where code_horraire='$codhoraire' group by classe";
                              $res=ejecutar_query($query,$link);
                            ?>
                           <div class="form-group input-group">
                                     
                                             <select class="form-control" name="classe" values="<?php echo $classe;?>">
                                                <option>Choisir une classe...</option>
                                               <?php for($i=0;$row=traer_fila($res);$i++){?>
                                                <option><?php echo $row[0];?></option>
                                               <?php }?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit" name="filtrer"><i class="fa fa-refresh"></i>
                                                 Charger Matiere
                                                </button>
                                            </span>
                                        </div>
                             <?php
                                if(isset($_POST['filtrer'])){
                                    $classe=$_POST['classe'];
									$query="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$classe' group by matiere ";
									$res=ejecutar_query($query,$link);
                                    $rows=traer_fila($res);
                                    
                                    $sql="select i.matricule from inscription i left join classe cl on cl.code_classe=i.code_classe where 
                                     i.annee_academique='$annee' and cl.description='$classe'";
                                    $wouch=ejecutar_query($sql,$link);
				               ?>
                            <hr>
                               <div class="col-lg-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                <i class="fa fa-users"></i> <label> Matricules par Classe / Contrôle*</label>
                                </div>
                                    <div class="panel-body">
                    
                                            <fieldset>
                                 <input class="form-control" placeholder="Classe" name="classes" type="text" value="<?php echo $rows[1];?>" readonly>
                                 <input class="form-control" placeholder="Classe" name="classes" type="hidden" value="<?php echo $rows[1];?>">
                                    <select class="form-control" name="controle" values="<?php echo $classe;?>">
                                      <option>Choisir un Contrôle...</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                    <select class="form-control" name="eleve" values="<?php echo $classe;?>">
                                      <option>Choisir le matricule de l'&eacute;l&egrave;ve...</option>
                                        <?php for($j=1;$srow=traer_fila($wouch);$j++) { ?>
                                        <option><?php echo $srow[0];?></option>
                                        <?php } ?>
                                    </select>
                                      </fieldset>

                                    </div>
                                </div>
                            </div>
        
                            <div class="row"></div>
                    
                              <div class="panel-body">
                                <input type="button" class="btn btn-default btn-lg btn-block" value="Liste des mati&egrave;res">
                                </div>  
                            
                            <!-- /.col-lg-4 -->
                            <?php 
                            $cmd="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$classe' group by matiere ";
									$rs=ejecutar_query($cmd,$link);
                                    
                                    
                            for($j=1,$i=1;$fil=traer_fila($rs);$j++,$i++) {
                            ?>
								<div class="col-lg-4">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <i class="fa fa-book"></i> <?php echo $fil[0];?>
                                    </div>
                                    <div class="panel-body">
                    
                                            <fieldset>
                                                <div class="form-group">
                                                    <label> Sur*</label>
                                                    <input class="form-control" placeholder="Sur" name="<?php echo "sur".$j?>" type="text" value="<?php echo $fil[2];?>" id="sur" readonly>
                                                    <input class="form-control" placeholder="Sur" name="<?php echo "sur".$j?>" type="hidden" id="sur" value="<?php echo $fil[2];?>" >
                                                </div>
                                                <div class="form-group">
                                                    <label> Note*</label>
                                                    <input class="form-control" placeholder="Note" id="note" name="<?php echo "note".$j;?>" type="text" autofocus onblur="konpare(sur,note)">
                                                </div>
                                            </fieldset>

                                    </div>
                                    
                                </div>
                             </div>
                                  <?php }?>
                                <div class="panel-body">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" name="publiez">Publiez</button>
                                </div>
                                <?php } ?>
                           </div>
                            <?php
                             if(isset($_POST['publiez'])){
                                 $klas=$_POST['classes'];
                                 $eleve=$_POST['eleve'];
                                 $controle=$_POST['controle'];
                                 $totalsur=0;$totalnote=0;$moyenne=0;
        $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
                         for($i=1;$liy=traer_fila($rs);$i++){
                             $teks=$_POST['sur'.$i];
                             $zone=$_POST['note'.$i];
                           $totalsur=$totalsur+$teks; 
                           $totalnote=$totalnote+$zone;
                         }        
    $dividande=$totalsur/10; $moyenne=$totalnote/$dividande;
    $moy=number_format($moyenne,2,".",".");
    
    $sql="select moyenne_aceptable from qualification where institution='$rejis[1]'";  
    $lol=ejecutar_query($sql,$link);
    $moys=$lol[0];
                                 
if(number_format($moyenne,2,".",".")>=9.00){
      $stat="Excéllent(te).";
  }else if(number_format($moyenne,2,".",".")<9.00 && number_format($moyenne,2,".",".")>=8.00){
      $stat="Très Bien.";
  }else if(number_format($moyenne,2,".",".")<8.00 && number_format($moyenne,2,".",".")>6.50){
      $stat="Assez Bien.";
  }else if(number_format($moyenne,2,".",".")==$moys){
     $stat="Bien.";
  }else {
      $stat="Mal.";
  }
                                 
 $kodklas=Buscar_Codigo("code_classe","description",$klas,"classe",$link);                        
 $string="insert into note(matricule,code_classe,total_note,total_sur,moyenne,status,controle,annee) values('$eleve','$kodklas','$totalnote','$totalsur','$moy','$stat','$controle','$annee')";
 $res=ejecutar_query($string,$link);
  // echo $string; 
                                 
  $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
         for($i=1;$liy=traer_fila($rs);$i++){
             $zone=$_POST['note'.$i];
             $teks=$_POST['sur'.$i];
             $controle=$_POST['controle'];
             $str="insert into detaille_note(matricule,matiere,note,sur,controle,annee,institution) values('$eleve','$liy[0]','$zone','$teks','$controle','$annee','$rejis[1]')";
             $res=ejecutar_query($str,$link);
            // echo $str; 
         }
                                 
        $cmd="select controle1,controle2,controle3,controle4 from temp where matricule='$eleve' and annee='$annee'";  
        $kouri=ejecutar_query($cmd,$link); 
        $kenbe=traer_fila($kouri);
                                 
        if($kenbe[0]==""){
           $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
         for($i=1;$liy=traer_fila($rs);$i++){
             $zone=$_POST['note'.$i];
             $teks=$_POST['sur'.$i];
             $controle=$_POST['controle'];
             $str="insert into temp(matricule,annee,matiere,sur,controle1,institution) values('$eleve','$annee','$liy[0]','$teks','$zone','$rejis[1]')";
             $res=ejecutar_query($str,$link);
             //echo $str; 
         } 
        }else if($kenbe[1]==""){
           $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
         for($i=1;$liy=traer_fila($rs);$i++){
             $zone=$_POST['note'.$i];
             $teks=$_POST['sur'.$i];
             $controle=$_POST['controle'];
             $str="update temp set controle2='$zone' where matricule='$eleve' and matiere='$liy[0]' and annee='$annee'";
             $res=ejecutar_query($str,$link);
            //echo $str; 
         } 
        }else if($kenbe[2]==""){
           $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
         for($i=1;$liy=traer_fila($rs);$i++){
             $zone=$_POST['note'.$i];
             $teks=$_POST['sur'.$i];
             $controle=$_POST['controle'];
             $str="update temp set controle3='$zone' where matricule='$eleve' and matiere='$liy[0]' and annee='$annee'";
             $res=ejecutar_query($str,$link);
            // echo $str; 
         } 
        }else if($kenbe[3]==""){
           $sql="select h.matiere, h.classe,e.valeur from  horaire h left join matiere m on m.description=h.matiere
        left join equivalence e on e.code_equivalence=m.code_equivalence where h.code_horraire='$codhoraire' and h.classe='$klas' group by matiere ";
        $rs=ejecutar_query($sql,$link);
         for($i=1;$liy=traer_fila($rs);$i++){
             $zone=$_POST['note'.$i];
             $teks=$_POST['sur'.$i];
             $controle=$_POST['controle'];
             $str="update temp set controle4='$zone' where matricule='$eleve' and matiere='$liy[0]' and annee='$annee'";
             $res=ejecutar_query($str,$link);
            // echo $str; 
         } 
        }
                                
                            }
                            ?>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
