<?php
include('lib/Classe.php');
$link=db_conectar();
session_start();
ob_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
	<title>Scolaris | R&eacute;servation</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post">
                 <div class="contact100-form-social flex-c-m">
					<a href="index.php" class="contact100-form-social-item flex-c-m bg1 m-r-5" title="Retournez-vous a la page d'accueil.">
						<i class="fa fa-home" aria-hidden="true"></i>
					</a>
				</div>
                
				<span class="contact100-form-title">
					R&eacute;servez votre inscription
				</span>

                
    <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Nom*" name="nom">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Prénom*" name="prenom">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select class="form-control" placeholder="Prénom*" name="sexe">
							<option>Sélectionner un Sèxe</option>
							<option>Féminin</option>
							<option>Masculin</option>
							<option>Autres</option>
							</select>
                             
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Date de Naissance*" name="naissance">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Adrèsse*" name="adresse">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Téléphone*" name="telephone">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select class="form-control" placeholder="Prénom*" name="sang">
							<option>Sélectionner un Groupe Sanguins</option>
							<option>A+</option>
							<option>A-</option>
							<option>B+</option>
							<option>B-</option>
							<option>AB-</option>
							<option>AB+</option>
							<option>O-</option>
							<option>O+</option>
							</select>
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select class="form-control" placeholder="Prénom*" name="condition">
							<option>Sélectionner une Condition de Santé</option>
							<option>Allèrgique</option>
							<option>Non Allèrgique</option>
							<option>Malaria</option>
							<option>Typhoïde</option>
							<option>Typho-Malaria</option>
							<option>Autres</option>
							</select>
                            </div>
                        </div>

						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Nom Complet du Responsable*" name="nomres">
                            </div>
                        </div>
                        
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Téléphone du Responsable*" name="telres">
                            </div>
                        </div>

						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select class="form-control" placeholder="Prénom*" name="lien">
							<option>Sélectionner un Lien Parenté</option>
							<option>Père</option>
							<option>Mère</option>
							<option>Frère</option>
							<option>Soeur</option>
							<option>Cousin</option>
							<option>Cousine</option>
							<option>Tante</option>
							<option>Oncle</option>
							<option>Autres</option>
							</select>
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
							<select class="form-control" placeholder="Prénom*" name="typeleve">
							<option>Sélectionner un Type d'élève</option>
							<option>Ancien(ne)Élève</option>
							<option>Nouveau(ne)Élève</option>
							</select>
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="matricule" id="matricule" placeholder="Matricule*" name="matricule">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Dernière établissement fréquanté*" name="etablissement">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Dernière Classe*" name="classe">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="nom" placeholder="Moyènne*" name="moyenne">
                            </div>
                        </div>
						
                
				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn" name="connect"  style="background-color:rgb(87,148,210);">
						Réserver
					</button>

                    
                     <?php
                        if(isset($_POST['connect'])){
                           // $matricules=$_POST['matricule'];
                           // $passwords=$_POST['password'];
                           // $query="select code,code_tercero,matricule,securite from employer where matricule=ltrim(rtrim('$matricules')) and securite=ltrim(rtrim('$passwords'))";
                           // $res=ejecutar_query($query,$link);
                           // $row=traer_fila($res);
                           // if ($passwords==$row[3]) {
                            //    session_start();
                           //     $_SESSION['matricule'] = $row[2];
                           //     header("Location: prive.php?matricule=$row[2]");
                           // } else {
                            //    header("Location: loginpv.php?matricule=$matricules");
                            //    exit();
                          //  }

                        }
                     ?>
				</div>
               				
			</form>

			<div class="contact100-more flex-col-c-m" style="background-image: url('images/rezevasyon.png');">
            </div>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
		$(".js-select2").each(function(){
			$(this).on('select2:open', function (e){
				$(this).parent().next().addClass('eff-focus-selection');
			});
		});
		$(".js-select2").each(function(){
			$(this).on('select2:close', function (e){
				$(this).parent().next().removeClass('eff-focus-selection');
			});
		});

	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/mainsoft.js"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-23581568-13');
	</script>
</body>
</html>
