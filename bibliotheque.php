<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$codhoraire='HorProf#'.$matricule;
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Liste</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select deviz from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[0]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-book"></i> T&eacute;l&eacute;chargement des documents 
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
                           
                            
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
							   <thead style="background-color:rgb(87,148,210);">
                                    <tr>
                                         <th  scope="col">&nbsp; Documents&nbsp;</th>
											<th width="114" scope="col">&nbsp;Tailles&nbsp;</th>
											<th width="50" scope="col">&nbsp;Format&nbsp;</th>
											<th width="50" scope="col">&nbsp;Action&nbsp;</th>
                                       
                                    </tr>
                                </thead>
								 
                                <tbody>
                                       <tr>
										<td>&nbsp;Adm Sistemas De Informacion&nbsp;</td>
										<td>&nbsp;16242 MB&nbsp;</td>
										<td>&nbsp;PDF&nbsp;<img src="images/PDF.png"/></td>
										<td >&nbsp;<a href="docs/Adm Sistemas De Informacion.pdf" target="_blank"><img src="images/view.png"/></a>&nbsp;</td>
										</tr>
										<tr>
										<td>&nbsp;Analisis de sistema de facturacion&nbsp;</td>
										<td>&nbsp;482052 KB&nbsp;</td>
										<td>&nbsp;PDF&nbsp;<img src="images/PDF.png"/></td>
										<td >&nbsp;<a href="docs/Analisis de sistema de facturacion.pdf" target="_blank"><img src="images/view.png"/></a>&nbsp;</td>
										</tr>
										<tr>
										<td>&nbsp;Arquitectura - Conceptos&nbsp;</td>
										<td>&nbsp;136584 KB&nbsp;</td>
										<td>&nbsp;PDF&nbsp;<img src="images/PDF.png"/></td>
										<td >&nbsp;<a href="docs/Arquitectura - Conceptos.pdf" target="_blank"><img src="images/view.png"/></a>&nbsp;</td>
										</tr>
                                </tbody>
								
                            </table>
                            
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
