<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
$codhoraire='HorProf#'.$matricule;
$query="select annee from annee_academique where code_annee=(select max(code_annee) from annee_academique)";
$res=ejecutar_query($query,$link);
$row=traer_fila($res);
$annee=$row[0];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Liste</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select deviz from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[0]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-users"></i> Liste des élèves
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
                            <?php
                            $query="select classe from horaire where code_horraire='$codhoraire' group by classe";
                            $res=ejecutar_query($query,$link);
                            ?>
                                
                                <div class="form-group input-group">
                                     
                                             <select class="form-control" name="classe" values="<?php echo $classe;?>">
                                                <option>Choisir une classe...</option>
                                               <?php for($i=0;$row=traer_fila($res);$i++){?>
                                                <option><?php echo $row[0];?></option>
                                               <?php }?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="submit" name="filtrer"><i class="fa fa-search"></i>
                                                </button>
                                            </span>
                                        </div>
                            
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
							   <thead style="background-color:rgb(87,148,210);">
                                    <tr>
                                        <th> &nbsp;Matricule&nbsp;</th>
                                        <th> &nbsp;Nom et Pr&eacute;nom&nbsp;</th>
										  <th> &nbsp;Classe&nbsp;</th>
                                       
                                    </tr>
                                </thead>
								 <?php
                                if(isset($_POST['filtrer'])){
                                $classe=$_POST['classe'];
									$query="select i.matricule,t.nom,t.prenom,cl.description as classe,i.annee_academique from inscription i left join tercero t
                                    on ((i.code = t.code) and (i.code_tercero=t.code_tercero)) left join classe cl on cl.code_classe = i.code_classe where cl.description='$classe' and i.annee_academique='$annee'";
                                     $querys="select count(i.matricule) as qte,cl.description from inscription i left join classe cl on cl.code_classe=i.code_classe where i.annee_academique='$annee' and cl.description='$classe'";
                                $ress=ejecutar_query($querys,$link);
                                $row=traer_fila($ress);
									$res=ejecutar_query($query,$link);
								   for($j=1;$rows=traer_fila($res);$j++)
								   {
								  ?>
                                <tbody>
                                    <tr class="odd gradeX">
                                        <td><?php echo $rows[0];?></td>
										<td><?php echo $rows[1]." ".$rows[2];?></td>
                                        <td><?php echo $rows[3];?></td>
                                        
                                    </tr>
                                </tbody>
								<?php }}?>
                            </table>
                            <?php
                             
                                echo "Quantité &Eacute;l&egrave;ves: ".$row[0];
                            ?>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
   

</body>

</html>
