<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
  <link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <title>Scolaris | Edition</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
	</head>
  <body>
    
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <?php include("includes/enteteprive.php"); ?>
  </nav>
    <!-- END nav -->

       <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
        			<div class="col-md-12 mb-5">
        				<div class="teacher-details d-md-flex">
        					<div class="img ftco-animate" ><?php include("includes/menu.php"); ?></div>
        					<div class="col-md-8">
								<form method="post" action="">
								  
									<table>
									  <thead>
										<tr>
										  <th>Changer de Mot de Passe</th>
										</tr>
									  </thead>
								
									  <tbody>
										<tr>
										</tr>
									  </tbody>
									</table>
										
										<div id="comments">

											  <div class="one_third first">
												<label for="name">Mot de Passe Actuel <span>*</span></label><br>
												<input type="password" name="actuel" id="actuel" value="" size="22" required>
											  </div>
											  <div class="one_third">
												<label for="email">Nouveau Mot de passe <span>*</span></label><br>
												<input type="password" name="nouveau" id="nouveau" value="" size="22" required>
											  </div>
											  <div class="one_third">
												<label for="email">Confirmar Mot de passe <span>*</span></label><br>
												<input type="password" name="confirmer" id="confirmer" value="" size="22" required>
											  </div>
											  
											  <div>
											  <br>
												<input type="submit" name="submit" value="Changer Mot de Passe">
									
											  </div>

										  </div>
										  <!-- ################################################################################################ -->
										</div>
										<?php
										if(isset($_POST['submit'])){
											$actuel=$_POST['actuel'];
											$nouveau=$_POST['nouveau'];
											$confirmer=$_POST['confirmer'];
											$sql="select securite from eleve where  matricule='$matricule'";
											$res=ejecutar_query($sql,$link);
											$row=traer_fila($res);
											if($row[0]==$actuel){
												if($nouveau==$confirmer){
													$query="update eleve set securite='$nouveau' where  matricule='$matricule' ";
													echo $query;
													$res=ejecutar_query($query,$link);
													if($res){
												echo'<label for="email" style="background-color:#FFFF99;">'.'<span style="color:green;">'."Le Mot de passe a été changer avec succès.".'</span>'.'</label>';
													}
												}else{
												echo'<label for="email" style="background-color:#FFFF99;">'."<span>"."Ouups!, La confirmation du mot de passe est incorrecte."."</span>".'</label>';	
												}
											}else{
											 echo'<label for="email" style="background-color:#FFFF99;">'."<span>"."Ouups!, Le Mot de passe actuel est incorrecte."."</span>".'</label>';	
											}
										}
										?>
									</form>
        					</div>
        				</div>
        			</div>
        			
        		</div>
        	</div>
        </div>
      </div>
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section img" style="background-image: url(images/bg_2.jpg); background-attachment:fixed;" >
    	<?php include("includes/pie.php"); ?>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>
    
  </body>
</html>