<?php
include('lib/Classe.php');
$link=db_conectar();
$matricule=$_GET['matricule'];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
<link href="images/icono.ico" type="image/x=icon" rel="shortcut icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Scolaris | Historique</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
           <?php include("includes/enteteprive.php"); ?>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
				<?php
				$query="select deviz from institution";
				//echo $query;
				$ress=ejecutar_query($query,$link);
				$rows=traer_fila($ress);
				?>
                    <h1 class="page-header" style="color:rgb(87,148,210);"><?php echo $rows[0]?></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                           <i class="fa fa-calendar"></i> Historique
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
						<form method="post" action="">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <?php
                                 $sql="select m.objet,m.messages,m.matricule,m.lien,date_format(m.dates,'%d %M %Y  /  %h:%m:%s') as dates,m.annee_academique,
                         m.dates as fecha from message m where m.matricule='$matricule'  order by dates desc";
                         $string=ejecutar_query($sql,$link);
                         for($i=1;$fil=traer_fila($string);$i++){
                         ?>
								 <tr>
                                     <td width="40">&nbsp;<a href="deletes.php?matricule=<?php echo $matricule;?>&root=<?php echo $fil[6];?>&lane=<?php echo $fil[5];?>&objet=<?php echo $fil[0];?>" target="_parent" title="Supprimer le message"><i class="fa fa-trash-o  "></i></a></td>
										<td>&nbsp;<?php echo $fil[0];?>&nbsp;</td>
										<td width="230">&nbsp;<?php echo $fil[4];?>&nbsp;</td>
										<td width="40">&nbsp;<a href="suivres.php?matricule=<?php echo $matricule;?>&root=<?php echo $fil[3];?>&lane=<?php echo $fil[1];?>&objet=<?php echo $fil[0];?>" target="_parent" title="Faire Suivre le message"><i class="fa fa-share"></i></a></td>
										<td width="40">&nbsp;<a href="reads.php?matricule=<?php echo $matricule;?>&root=<?php echo $fil[3];?>&lane=<?php echo $fil[5];?>&objet=<?php echo $fil[0];?>" target="_parent" title="Lire le message"><i class="fa fa-eye"></i></a>&nbsp;</td>
								</tr>
                                  <?php }?>
                                <tbody>
                             
                            </table>
							</form>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
         
            <!-- /.row -->
           
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    

</body>

</html>
